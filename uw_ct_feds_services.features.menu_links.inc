<?php

/**
 * @file
 * uw_ct_feds_services.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_feds_services_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_feds-services-categories:admin/structure/taxonomy/feds_services_categories.
  $menu_links['menu-site-manager-vocabularies_feds-services-categories:admin/structure/taxonomy/feds_services_categories'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/feds_services_categories',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Feds services categories',
    'options' => array(
      'attributes' => array(
        'title' => 'Categories apply to a specific Feds service.',
      ),
      'identifier' => 'menu-site-manager-vocabularies_feds-services-categories:admin/structure/taxonomy/feds_services_categories',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds services categories');

  return $menu_links;
}
