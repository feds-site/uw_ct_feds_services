<?php

/**
 * @file
 * uw_ct_feds_services.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_services_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer feeds'.
  $permissions['administer feeds'] = array(
    'name' => 'administer feeds',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'feeds',
  );

  // Exported permission: 'create feds_service_directory content'.
  $permissions['create feds_service_directory content'] = array(
    'name' => 'create feds_service_directory content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in feds_services_categories'.
  $permissions['define view for terms in feds_services_categories'] = array(
    'name' => 'define view for terms in feds_services_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_services_categories'.
  $permissions['define view for vocabulary feds_services_categories'] = array(
    'name' => 'define view for vocabulary feds_services_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any feds_service_directory content'.
  $permissions['delete any feds_service_directory content'] = array(
    'name' => 'delete any feds_service_directory content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_service_directory content'.
  $permissions['delete own feds_service_directory content'] = array(
    'name' => 'delete own feds_service_directory content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in feds_services_categories'.
  $permissions['delete terms in feds_services_categories'] = array(
    'name' => 'delete terms in feds_services_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any feds_service_directory content'.
  $permissions['edit any feds_service_directory content'] = array(
    'name' => 'edit any feds_service_directory content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_service_directory content'.
  $permissions['edit own feds_service_directory content'] = array(
    'name' => 'edit own feds_service_directory content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in feds_services_categories'.
  $permissions['edit terms in feds_services_categories'] = array(
    'name' => 'edit terms in feds_services_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter feds_service_directory revision log entry'.
  $permissions['enter feds_service_directory revision log entry'] = array(
    'name' => 'enter feds_service_directory revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_service_directory authored by option'.
  $permissions['override feds_service_directory authored by option'] = array(
    'name' => 'override feds_service_directory authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_service_directory authored on option'.
  $permissions['override feds_service_directory authored on option'] = array(
    'name' => 'override feds_service_directory authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_service_directory promote to front page option'.
  $permissions['override feds_service_directory promote to front page option'] = array(
    'name' => 'override feds_service_directory promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_service_directory published option'.
  $permissions['override feds_service_directory published option'] = array(
    'name' => 'override feds_service_directory published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_service_directory revision option'.
  $permissions['override feds_service_directory revision option'] = array(
    'name' => 'override feds_service_directory revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_service_directory sticky option'.
  $permissions['override feds_service_directory sticky option'] = array(
    'name' => 'override feds_service_directory sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_service_directory content'.
  $permissions['search feds_service_directory content'] = array(
    'name' => 'search feds_service_directory content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
