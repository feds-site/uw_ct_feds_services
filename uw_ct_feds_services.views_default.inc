<?php

/**
 * @file
 * uw_ct_feds_services.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_services_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_service_hours';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds services';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'WUSA Services';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
    ),
    'field_feds_service_category_tid' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => 0,
        'bef_select_all_none_nested' => 0,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Service Logo */
  $handler->display->display_options['fields']['field_service_logo']['id'] = 'field_service_logo';
  $handler->display->display_options['fields']['field_service_logo']['table'] = 'field_data_field_service_logo';
  $handler->display->display_options['fields']['field_service_logo']['field'] = 'field_service_logo';
  $handler->display->display_options['fields']['field_service_logo']['label'] = '';
  $handler->display->display_options['fields']['field_service_logo']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_service_logo']['element_class'] = 'services_logo';
  $handler->display->display_options['fields']['field_service_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_service_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_service_logo']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: Content: Service Page Link */
  $handler->display->display_options['fields']['field_service_page_link']['id'] = 'field_service_page_link';
  $handler->display->display_options['fields']['field_service_page_link']['table'] = 'field_data_field_service_page_link';
  $handler->display->display_options['fields']['field_service_page_link']['field'] = 'field_service_page_link';
  $handler->display->display_options['fields']['field_service_page_link']['label'] = '';
  $handler->display->display_options['fields']['field_service_page_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_service_page_link']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_service_page_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_service_page_link']['type'] = 'node_reference_path';
  $handler->display->display_options['fields']['field_service_page_link']['settings'] = array(
    'alias' => 1,
    'absolute' => 0,
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<a href="[field_service_page_link]">[title]</a>';
  $handler->display->display_options['fields']['title']['alter']['path_case'] = 'lower';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_class'] = 'no-underline';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: First Name */
  $handler->display->display_options['fields']['field_feds_first_name']['id'] = 'field_feds_first_name';
  $handler->display->display_options['fields']['field_feds_first_name']['table'] = 'field_data_field_feds_first_name';
  $handler->display->display_options['fields']['field_feds_first_name']['field'] = 'field_feds_first_name';
  $handler->display->display_options['fields']['field_feds_first_name']['exclude'] = TRUE;
  /* Field: Content: Email */
  $handler->display->display_options['fields']['field_services_email']['id'] = 'field_services_email';
  $handler->display->display_options['fields']['field_services_email']['table'] = 'field_data_field_services_email';
  $handler->display->display_options['fields']['field_services_email']['field'] = 'field_services_email';
  $handler->display->display_options['fields']['field_services_email']['label'] = '';
  $handler->display->display_options['fields']['field_services_email']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_services_email']['element_class'] = 'services_email';
  $handler->display->display_options['fields']['field_services_email']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_services_email']['element_wrapper_class'] = 'fx-item';
  $handler->display->display_options['fields']['field_services_email']['hide_empty'] = TRUE;
  /* Field: Content: Phone Number */
  $handler->display->display_options['fields']['field_services_phone']['id'] = 'field_services_phone';
  $handler->display->display_options['fields']['field_services_phone']['table'] = 'field_data_field_services_phone';
  $handler->display->display_options['fields']['field_services_phone']['field'] = 'field_services_phone';
  $handler->display->display_options['fields']['field_services_phone']['label'] = '';
  $handler->display->display_options['fields']['field_services_phone']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_services_phone']['element_class'] = 'services_phonenumber';
  $handler->display->display_options['fields']['field_services_phone']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_services_phone']['element_wrapper_class'] = 'fx-item';
  $handler->display->display_options['fields']['field_services_phone']['hide_empty'] = TRUE;
  /* Field: Content: Regular Hours */
  $handler->display->display_options['fields']['field_regular_hours']['id'] = 'field_regular_hours';
  $handler->display->display_options['fields']['field_regular_hours']['table'] = 'field_data_field_regular_hours';
  $handler->display->display_options['fields']['field_regular_hours']['field'] = 'field_regular_hours';
  $handler->display->display_options['fields']['field_regular_hours']['label'] = 'Hours';
  $handler->display->display_options['fields']['field_regular_hours']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['field_regular_hours']['element_label_type'] = 'h4';
  $handler->display->display_options['fields']['field_regular_hours']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_regular_hours']['element_wrapper_class'] = 'services_regular_hours fx-item';
  $handler->display->display_options['fields']['field_regular_hours']['hide_empty'] = TRUE;
  /* Field: Content: Special Hours & Notifications */
  $handler->display->display_options['fields']['field_special_hours_notification']['id'] = 'field_special_hours_notification';
  $handler->display->display_options['fields']['field_special_hours_notification']['table'] = 'field_data_field_special_hours_notification';
  $handler->display->display_options['fields']['field_special_hours_notification']['field'] = 'field_special_hours_notification';
  $handler->display->display_options['fields']['field_special_hours_notification']['alter']['nl2br'] = TRUE;
  $handler->display->display_options['fields']['field_special_hours_notification']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_special_hours_notification']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_special_hours_notification']['element_wrapper_class'] = 'services_notification';
  $handler->display->display_options['fields']['field_special_hours_notification']['hide_empty'] = TRUE;
  /* Field: Content: Service Category */
  $handler->display->display_options['fields']['field_feds_service_category']['id'] = 'field_feds_service_category';
  $handler->display->display_options['fields']['field_feds_service_category']['table'] = 'field_data_field_feds_service_category';
  $handler->display->display_options['fields']['field_feds_service_category']['field'] = 'field_feds_service_category';
  $handler->display->display_options['fields']['field_feds_service_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_feds_service_category']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_feds_service_category']['delta_offset'] = '0';
  /* Sort criterion: Content: Title (title_field) */
  $handler->display->display_options['sorts']['title_field_value']['id'] = 'title_field_value';
  $handler->display->display_options['sorts']['title_field_value']['table'] = 'field_data_title_field';
  $handler->display->display_options['sorts']['title_field_value']['field'] = 'title_field_value';
  /* Sort criterion: Content: Title (title_field:language) */
  $handler->display->display_options['sorts']['language']['id'] = 'language';
  $handler->display->display_options['sorts']['language']['table'] = 'field_data_title_field';
  $handler->display->display_options['sorts']['language']['field'] = 'language';
  /* Contextual filter: Content: Has taxonomy term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['exception']['title'] = '%1';
  $handler->display->display_options['arguments']['tid']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['tid']['title'] = '%1';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
    'feds_services_categories' => 'feds_services_categories',
  );
  $handler->display->display_options['arguments']['tid']['validate_options']['type'] = 'convert';
  $handler->display->display_options['arguments']['tid']['validate_options']['transform'] = TRUE;
  $handler->display->display_options['arguments']['tid']['require_value'] = TRUE;
  $handler->display->display_options['arguments']['tid']['set_breadcrumb'] = TRUE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_service_directory' => 'feds_service_directory',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: full service listing */
  $handler = $view->new_display('page', 'full service listing', 'feds_full_service_listing');
  $handler->display->display_options['display_description'] = 'Directory of all the WUSA available services.';
  $handler->display->display_options['path'] = 'services';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Feds Services';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Services - food and retail. */
  $handler = $view->new_display('page', 'Services - food and retail.', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Food & Retail Services';
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['display_description'] = 'Directory of Feds services associated to food and retail.';
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Has taxonomy term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['type'] = 'taxonomy_term';
  $handler->display->display_options['arguments']['tid']['validate_options']['vocabularies'] = array(
    'feds_services_categories' => 'feds_services_categories',
  );
  $handler->display->display_options['arguments']['tid']['validate_options']['type'] = 'convert';
  $handler->display->display_options['arguments']['tid']['validate_options']['transform'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'ignore';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_service_directory' => 'feds_service_directory',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  $handler->display->display_options['path'] = 'services-food-retail';
  $handler->display->display_options['menu']['title'] = 'Feds Services';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Services - health and wellness */
  $handler = $view->new_display('page', 'Services - health and wellness', 'page_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Health & Wellness Services';
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['display_description'] = 'Directory of Feds services associated to health and wellness.';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_service_directory' => 'feds_service_directory',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Service Category (field_feds_service_category) */
  $handler->display->display_options['filters']['field_feds_service_category_tid']['id'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['table'] = 'field_data_field_feds_service_category';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['field'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['value'] = array(
    62 => '62',
  );
  $handler->display->display_options['filters']['field_feds_service_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['vocabulary'] = 'feds_services_categories';
  $handler->display->display_options['path'] = 'services-health-wellness';
  $handler->display->display_options['menu']['title'] = 'Feds Services';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Services - commercial */
  $handler = $view->new_display('page', 'Services - commercial', 'page_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Commercial Services';
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['display_description'] = 'Directory of Feds services associated to the business operations.';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_service_directory' => 'feds_service_directory',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Service Category (field_feds_service_category) */
  $handler->display->display_options['filters']['field_feds_service_category_tid']['id'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['table'] = 'field_data_field_feds_service_category';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['field'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['value'] = array(
    118 => '118',
  );
  $handler->display->display_options['filters']['field_feds_service_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['vocabulary'] = 'feds_services_categories';
  $handler->display->display_options['path'] = 'services-commercial';
  $handler->display->display_options['menu']['title'] = 'Feds Services';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Services - student */
  $handler = $view->new_display('page', 'Services - student', 'page_4');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Student Services';
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['display_description'] = 'Directory of Feds services associated to student services.';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_service_directory' => 'feds_service_directory',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Service Category (field_feds_service_category) */
  $handler->display->display_options['filters']['field_feds_service_category_tid']['id'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['table'] = 'field_data_field_feds_service_category';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['field'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['value'] = array(
    111 => '111',
  );
  $handler->display->display_options['filters']['field_feds_service_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['vocabulary'] = 'feds_services_categories';
  $handler->display->display_options['path'] = 'services-student';
  $handler->display->display_options['menu']['title'] = 'Feds Services';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Services - transportation */
  $handler = $view->new_display('page', 'Services - transportation', 'page_5');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Transportation Services';
  $handler->display->display_options['enabled'] = FALSE;
  $handler->display->display_options['display_description'] = 'Directory of Feds services associated to transportation services.';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_service_directory' => 'feds_service_directory',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Service Category (field_feds_service_category) */
  $handler->display->display_options['filters']['field_feds_service_category_tid']['id'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['table'] = 'field_data_field_feds_service_category';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['field'] = 'field_feds_service_category_tid';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['value'] = array(
    64 => '64',
  );
  $handler->display->display_options['filters']['field_feds_service_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_feds_service_category_tid']['vocabulary'] = 'feds_services_categories';
  $handler->display->display_options['path'] = 'services-transportation';
  $handler->display->display_options['menu']['title'] = 'Feds Services';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Service Feed */
  $handler = $view->new_display('feed', 'Service Feed', 'feed_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['row_options']['item_length'] = 'full';
  $handler->display->display_options['path'] = 'wusa-service-feed';
  $translatables['feds_service_hours'] = array(
    t('Master'),
    t('WUSA Services'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Advanced options'),
    t('<a href="[field_service_page_link]">[title]</a>'),
    t('First Name'),
    t('Hours'),
    t('Special Hours & Notifications'),
    t('Service Category'),
    t('%1'),
    t('full service listing'),
    t('Directory of all the WUSA available services.'),
    t('Services - food and retail.'),
    t('Food & Retail Services'),
    t('Directory of Feds services associated to food and retail.'),
    t('All'),
    t('Services - health and wellness'),
    t('Health & Wellness Services'),
    t('Directory of Feds services associated to health and wellness.'),
    t('Services - commercial'),
    t('Commercial Services'),
    t('Directory of Feds services associated to the business operations.'),
    t('Services - student'),
    t('Student Services'),
    t('Directory of Feds services associated to student services.'),
    t('Services - transportation'),
    t('Transportation Services'),
    t('Directory of Feds services associated to transportation services.'),
    t('Service Feed'),
  );
  $export['feds_service_hours'] = $view;

  return $export;
}
