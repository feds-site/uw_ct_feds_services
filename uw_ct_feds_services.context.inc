<?php

/**
 * @file
 * uw_ct_feds_services.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_feds_services_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_service_commercial';
  $context->description = 'Feds commercial services view';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'clubs-services/services/commercial-services' => 'clubs-services/services/commercial-services',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_service_hours-block_6' => array(
          'module' => 'views',
          'delta' => 'feds_service_hours-block_6',
          'region' => 'secondary_content',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Feds commercial services view');
  $export['feds_service_commercial'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_service_directory';
  $context->description = 'Listing of all Feds Services';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'clubs-services/services' => 'clubs-services/services',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_service_hours-block' => array(
          'module' => 'views',
          'delta' => 'feds_service_hours-block',
          'region' => 'secondary_content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Listing of all Feds Services');
  $export['feds_service_directory'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_service_foodretail';
  $context->description = 'Listing of Feds services within the food and retail category.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'clubs-services/services/food-and-retail' => 'clubs-services/services/food-and-retail',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_service_hours-block_2' => array(
          'module' => 'views',
          'delta' => 'feds_service_hours-block_2',
          'region' => 'secondary_content',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Listing of Feds services within the food and retail category.');
  $export['feds_service_foodretail'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_service_health_and_wellness';
  $context->description = 'Listing of Feds services within the health and wellness category.';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'clubs-services/services/health-and-wellness' => 'clubs-services/services/health-and-wellness',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_service_hours-block_3' => array(
          'module' => 'views',
          'delta' => 'feds_service_hours-block_3',
          'region' => 'secondary_content',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Listing of Feds services within the health and wellness category.');
  $export['feds_service_health_and_wellness'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_service_studentservices';
  $context->description = 'Listing of Feds services categorized as a "Student service".';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'clubs-services/services/student-services' => 'clubs-services/services/student-services',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_service_hours-block_5' => array(
          'module' => 'views',
          'delta' => 'feds_service_hours-block_5',
          'region' => 'secondary_content',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Listing of Feds services categorized as a "Student service".');
  $export['feds_service_studentservices'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'feds_service_transportation';
  $context->description = 'Listing of Feds services within the "Transportation" category';
  $context->tag = 'Feds';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'clubs-services/services/transportation' => 'clubs-services/services/transportation',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-feds_service_hours-block_1' => array(
          'module' => 'views',
          'delta' => 'feds_service_hours-block_1',
          'region' => 'secondary_content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds');
  t('Listing of Feds services within the "Transportation" category');
  $export['feds_service_transportation'] = $context;

  return $export;
}
