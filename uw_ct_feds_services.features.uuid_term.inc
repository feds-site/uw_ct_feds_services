<?php

/**
 * @file
 * uw_ct_feds_services.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_feds_services_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Health and Wellness',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '0274137d-2b99-450a-8ccd-f45407cfc39d',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_services_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-services-categories/health-and-wellness',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Food and Retail',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '446ee663-b205-4a5e-9b17-05c88cf3f0a0',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_services_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-services-categories/food-and-retail',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Transportation',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => 'a5437dd4-ade7-4792-ac70-054240f1897b',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_services_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-services-categories/transportation',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Environmental',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => 'b54fb5b1-bd28-4e6e-84bb-58c4ef8b4d23',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_services_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-services-categories/environmental',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Commercial Services',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => 'ba60f1ca-7d02-4bf2-a66b-d86d835f3137',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_services_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-services-categories/commercial-services',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Student Service',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => 'e5b8e2fa-fb7e-470d-95db-3548d775356e',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_services_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-services-categories/student-service',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
